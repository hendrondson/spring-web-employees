package com.citi.training.employees.dao;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.citi.training.employees.exceptions.EmployeeNotFoundException;
import com.citi.training.employees.model.Employee;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MySqlEmployeeDaoTests {

	@Autowired
	MySqlEmployeeDao mySqlEmployeeDao;

	@Test
	@Transactional
	public void test_createAndFindAll() {
		mySqlEmployeeDao.create(new Employee(-1, "Frank", 10.0));

		assertEquals(mySqlEmployeeDao.findAll().size(), 1);
	}

	@Test
	@Transactional
	public void test_findById() {
		mySqlEmployeeDao.create(new Employee(1, "Kyle", 10.0));

		assertEquals(mySqlEmployeeDao.findById(25).getId(), 25);
	}

	@Test(expected = EmployeeNotFoundException.class)
	@Transactional
	public void test_deleteById() {
		Employee employee = new Employee(-1, "Kyle", 10.0);
		mySqlEmployeeDao.create(employee);

		mySqlEmployeeDao.deleteById(1);
		mySqlEmployeeDao.findById(1);
	}

}
