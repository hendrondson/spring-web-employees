package com.citi.training.employees.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.function.ToLongBiFunction;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Component;

import com.citi.training.employees.exceptions.EmployeeNotFoundException;
import com.citi.training.employees.model.Employee;

@Component
public class MySqlEmployeeDao implements EmployeeDao {

	@Autowired
	JdbcTemplate tpl;

	@Override
	public List<Employee> findAll() {
		return tpl.query("SELECT id, name, salary FROM employee", new EmployeeMapper());

	}

	private static final class EmployeeMapper implements RowMapper<Employee> {

		@Override
		public Employee mapRow(ResultSet rs, int rowNum) throws SQLException {
			return new Employee(rs.getInt("id"), rs.getString("name"), rs.getDouble("salary"));
		}

	}

	@Override
	public Employee findById(int id) {
		List<Employee> employees = tpl.query("SELECT id, name, salary FROM employee WHERE id = ?", 
								new Object[] {id}, 
								new EmployeeMapper());
		
		if(employees.size() <= 0) {
			throw new EmployeeNotFoundException("Employee with id=[" + id + "] not found");
		}
		
		return employees.get(0);
	}

	@Override
	public Employee create(Employee employee) {
		KeyHolder keyHolder = new GeneratedKeyHolder();

		this.tpl.update(new PreparedStatementCreator() {
			@Override
			public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
				PreparedStatement ps = connection.prepareStatement("insert into employee (name, salary) values (?, ?)",
						Statement.RETURN_GENERATED_KEYS);
				ps.setString(1, employee.getName());
				ps.setDouble(2, employee.getSalary());
				return ps;
			}
		}, keyHolder);

		employee.setId(keyHolder.getKey().intValue());
		return employee;
	}

	@Override
	public void deleteById(int id) {
		findById(id);
		tpl.update("DELETE FROM employee WHERE id = ? ", id);

	}

}
